package com.spring.boot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.spring.boot.model.Topic;

public interface TopicRepository extends MongoRepository<Topic, Integer> {

}
