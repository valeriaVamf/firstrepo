package com.spring.boot.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.boot.model.Topic;
import com.spring.boot.repository.TopicRepository;

@Service
public class TopicService {
	
	@Autowired
	private TopicRepository repository;

	private List<Topic> topics = new ArrayList<> (Arrays.asList(
			new  Topic(1, "name1", "desc1"),
			new  Topic(2, "name2", "desc2"),	
			new  Topic(3, "name3", "desc3")	
			));

	public List<Topic> getAllTopics() {
		//return topics;
		List<Topic> result = new ArrayList<>();
		repository.findAll().forEach(result :: add);
		return result;
	}

	public Topic getTopicById(int id) {
		//return topics.stream().filter(t -> t.getId() == id).findFirst().get();
		return repository.findOne(id);
	}

	public void addTopic(Topic topic) {
		//topics.add(topic);
		repository.save(topic);
		
	}

	public void updateTopic(int id, Topic topic) {
//		for(int i = 0; i<topics.size();i++){
//			Topic aTopic= topics.get(i);
//			if(aTopic.getId() == id){
//				topics.set(i, topic);
//				return;
//			}
//		}
		
		repository.save(topic);
		
	}

	public void deleteTopic(int id) {
		//topics.removeIf(t -> t.getId() == id);
		repository.delete(id);
		
	}
	
	
}
