package com.spring.boot.rest.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.model.Topic;
import com.spring.boot.service.TopicService;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService service;

	@RequestMapping("topic")
	public List<Topic> returAllTopics(){
		return  service.getAllTopics();
	}
	
	@RequestMapping("topic/{id}")// topic/{foo}
	public Topic retriveTopicById(@PathVariable int id){// @PathVariable("foo") int id
		return  service.getTopicById(id);
	}
	
	@RequestMapping(method = RequestMethod.POST ,value="topic")
	public void addTopic(@RequestBody Topic topic){
		  service.addTopic(topic);
	}
	
	@RequestMapping(method = RequestMethod.PUT ,value="topic/{id}")
	public void updateTopic(@PathVariable int id, @RequestBody Topic topic){
		  service.updateTopic(id,topic);
	}
	
	@RequestMapping(method = RequestMethod.DELETE ,value="topic/{id}")
	public void deleteTopic(@PathVariable int id){
		  service.deleteTopic(id);
	}
}
