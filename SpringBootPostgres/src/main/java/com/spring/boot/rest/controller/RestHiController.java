package com.spring.boot.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class RestHiController {

	@RequestMapping("hello")
	public String sayHi(){
		return "Hi";
	}
}
